<?php

defined('JPATH_BASE') or die;


class plgUserProfileAnnuaire extends JPlugin
{
    private $profilekey = "profileannuaire";


    //Chargement des données
    function onContentPrepareData($context, $data)
    {
        if (!in_array($context, array('com_users.profile','com_users.registration','com_users.user','com_admin.profile'))){
            return true;
        }

        $user_id = isset($data->id) ? $data->id : 0;

        //Chargement du profil
        $db = JFactory::getDbo();
        $db->setQuery("SELECT profile_key, profile_value FROM #__user_profiles WHERE user_id = ".(int)$user_id." AND profile_key LIKE '$this->profilekey.%' ORDER BY ordering");

        $results = $db->loadRowList();




        $data->profileannuaire = array();

        foreach($results as $value)
        {
            $key = str_replace('profileannuaire.', '', $value[0]);
            $data->profileannuaire[$key] = json_decode($value[1], true);
        }



    }

    //Chargement du formulaire
    function onContentPrepareForm($form, $data)
    {

        $lang = JFactory::getLanguage();
        $lang->load('plg_user_annuaire', JPATH_ADMINISTRATOR);

        if (!($form instanceof JForm)) {
            $this->_subject->setError('JERROR_NOT_A_FORM');
            return false;
        }

        if (!in_array($form->getName(), array('com_users.profile', 'com_users.registration','com_users.user','com_admin.profile'))) {
            return true;
        }

        JForm::addFormPath(__DIR__ . '/profiles');
        $form->loadFile('profile', false);


        $fields = array(
            "annuaire_address",
            "annuaire_ville",
            "annuaire_formation",
            "annuaire_description",
            "annuaire_promotion",
            "annuaire_pays"
        );

        $name = $form->getName();

        foreach ($fields as $field) {

            //Administration d'un utilisateur
            if ($name == 'com_users.user')
            {

                // Suppression du champ si il est désactivé pour l'inscription et la vue du profil
                if ($this->params->get('register-require_' . $field, 1) == 0
                    && $this->params->get('profile-require_' . $field, 1) == 0
                )
                {

                    $form->removeField($field, $this->profilekey);
                }
            }
            //Formulaire d'inscription
            elseif ($name == 'com_users.registration')
            {
                // Toggle whether the field is required.
                if ($this->params->get('register-require_' . $field, 1) > 0)
                {
                    $form->setFieldAttribute($field, 'required', ($this->params->get('register-require_' . $field) == 2) ? 'required' : '', $this->profilekey);
                }
                else
                {
                    $form->removeField($field, $this->profilekey);
                }
            }
            // Affichage profil sur le frontend et le backend
            elseif ($name == 'com_users.profile' || $name == 'com_admin.profile')
            {
                // ajoute l'attribut "required" si besoin
                if ($this->params->get('profile-require_' . $field, 1) > 0)
                {
                    $form->setFieldAttribute($field, 'required', ($this->params->get('profile-require_' . $field) == 2) ? 'required' : '', $this->profilekey);
                }
                else
                {
                    $form->removeField($field, $this->profilekey);
                }
            }

        }

        return true;


    }
    //Sauvegarde du profil dans la base
	function onUserAfterSave($data, $isNew, $result, $error)
    {
        $user_id = $data['id'];

        $params = $data[$this->profilekey];

        if ($user_id && $result && isset($data[$this->profilekey]) && count($data[$this->profilekey])) {
            $db = JFactory::getDbo();

            //Suppression des anciens paramètres
            $db->setQuery("DELETE FROM #__user_profiles WHERE user_id = $user_id AND profile_key LIKE 'profileannuaire.%'");

            if (!$db->query())
                throw new Exception($db->getErrorMsg());

            $tuples = array();
            $order = 1;

            foreach ($params as $key => $value) {
                $tuples[] = '(' . $user_id . ',' . $db->quote($this->profilekey . '.' . $key) . ',' . $db->quote(json_encode($value)) . ',' . $order++ . ')';
            }

            $db->setQuery("INSERT INTO #__user_profiles VALUES " . implode(', ', $tuples));

            if (!$db->query())
                throw new Exception($db->getErrorMsg());


            return true;
        }
    }

        //Lorsque l'utilisateur est supprimé
    function onUserAfterDelete($user, $success, $msg)
    {

        if(!$success)
            return false;

        $user_id = JArrayHelper::getValue($user, 'id', 0, 'int');

        if($user_id)
        {
            $db = JFactory::getDbo();

            $db->setQuery("DELETE FROM #__user_profiles WHERE user_id = ".$user_id." AND profile_key LIKE '$this->profilekey.%'");

            if(!$db->query())
            {
                return false;
            }
        }

        return true;
    }








}